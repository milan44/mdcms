<?php
require_once dirname(__FILE__) . '/_inc/Classes/IntegrityChecker.php';
IntegrityChecker::check();

require_once dirname(__FILE__) . '/_inc/Classes/ExceptionHandler.php';
ExceptionHandler::registerHandlers();

require_once dirname(__FILE__) . '/_inc/functions.php';

$url = removeExtension($_SERVER['REQUEST_URI']);

if (isset($_GET['path']) && !empty(GLOBAL_CONFIG['ignoreCacheParameter'])) {
    $url = $_GET['path'];
}

if (empty($url)) {
    $url = '/';
}
if (endsWith($url, '/') || $url === '/') {
    if (isset(GLOBAL_CONFIG['directoryIndex'])) {
        $url .= removeExtension(GLOBAL_CONFIG['directoryIndex']);
    } else {
        $url .= 'index';
    }
}

$url = str_replace('//', '/', $url);

if (isset($_GET['asset'])) {
    try {
        AssetRenderer::renderAsset($_GET['asset']);
    } catch (Exception $e) {
        ExceptionHandler::handle($e, dirname(__FILE__));
    }
}

if (startsWith($url, '/_inc')) {
    header('Content-type: text/plain');
    die('Access denied');
}

$markdownFile = dirname(__FILE__) . $url . '.md';
$cacheFile = dirname(__FILE__) . $url . '.html';

if (!empty(GLOBAL_CONFIG['mappings']) && !empty(GLOBAL_CONFIG['mappings'][$url])) {
    $markdownFile = dirname(__FILE__) . '/' . GLOBAL_CONFIG['mappings'][$url];
}

try {
    $renderer = new FileRenderer($markdownFile);
    $html = $renderer->render();

    file_put_contents($cacheFile, $html);
    die($html);
} catch (Exception $e) {
    ExceptionHandler::handle($e, dirname(__FILE__));
}