# MdCMS

Flat File, high speed Markdown CMS.

### Installation

Drop the files in your directory and change the values in `_inc/config.php` to fit your needs.
Then you can create your directory structure and markdown files. You can also include static html files.  

---

You can change the layout of the page (where stuff goes) by creating your own theme.
You can copy the default theme (`_inc/themes/default`) and then change the `_inc/themes/yourtheme/layout.html`.
`###HEAD###` for example is a placeholder and will be replaced by the contents of the `_inc/themes/yourtheme/head.html` file.

The theme folder also includes all partials that will be included in the rendered HTML. You can change them to fit your needs.

To use your theme you need to set the `'theme'` key in the `_inc/config.php` file.
`'theme' => 'wiese2'` for example will use the folder `_inc/themes/wiese2` as the theme.

---

Every variable in the `site` array inside the `_inc/config.php` file will be available in your markdown surrounded with {{ and }}.

**Example:**

```
...
'site' => [
    'test' => 'My Title',
    'sub' => [
        'test' => 'hello'
    ]
],
...
```

Will make the following variables available in every markdown file:
```
{{test}} -> 'My Title'
{{sub.test}} -> 'hello'
```

If you want to override global variables in one of your markdown files, you can do so my adding them as json at the top of your markdown file, followed by a line containing **only** `---`.

**Example:**

hello.md
```
{
    "test": "Hello"
}
---
# This is my markdown page
```

Will override the `{{test}}` variable with the value `Hello` for that page.