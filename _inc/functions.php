<?php

require_once dirname(__FILE__) . '/vendor/parsedown/Parsedown.php';
require_once dirname(__FILE__) . '/vendor/html-minify/TinyHtmlMinifier.php';
require_once dirname(__FILE__) . '/config.php';

require_once dirname(__FILE__) . '/Classes/CMSException.php';
require_once dirname(__FILE__) . '/Classes/ConfigurationException.php';
require_once dirname(__FILE__) . '/Classes/FileDoesntExistException.php';
require_once dirname(__FILE__) . '/Classes/SeoOptimizer.php';
require_once dirname(__FILE__) . '/Classes/FileRenderer.php';
require_once dirname(__FILE__) . '/Classes/AssetRenderer.php';
require_once dirname(__FILE__) . '/Classes/ExceptionHandler.php';
require_once dirname(__FILE__) . '/Classes/AdminResponse.php';
require_once dirname(__FILE__) . '/Classes/AdminPanel.php';

/**
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function startsWith(string $haystack, string $needle): bool
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function endsWith(string $haystack, string $needle): bool
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

/**
 * @param string $file
 * @return string
 */
function removeExtension(string $file): string
{
    if (!strrpos($file, ".")) {
        return $file;
    }
    return trim(substr($file, 0, strrpos($file, ".")));
}

/**
 * @param string $msg
 */
function mdcms_log(string $msg)
{
    echo date('[d.m.Y - H:i:s] ') . $msg . "\n";
}

/**
 * @param string $path
 * @return mixed
 */
function normalizePath(string $path)
{
    return array_reduce(explode('/', $path), function ($a, $b) {
        if ($a === 0)
            $a = "/";

        if ($b === "" || $b === ".")
            return $a;

        if ($b === "..")
            return dirname($a);

        return preg_replace("/\/+/", "/", "$a/$b");
    }, 0);
}

function shouldBeIgnored(string $file): bool
{
    $file = realpath($file);

    foreach (GLOBAL_CONFIG['ignoreCache'] as $ignore) {
        $ignore = realpath(__DIR__ . '/../' . $ignore);

        if ($ignore === $file) {
            return true;
        }
    }

    return false;
}

/**
 * @param $microseconds
 * @return string
 */
function formatMicroseconds($microseconds): string
{
    $milliseconds = floor($microseconds * 1000);
    $microseconds = floor(($microseconds * 1000 * 1000) - ($milliseconds * 1000));
    return ($milliseconds > 0 ? $milliseconds . " milliseconds and " : "") . $microseconds . " microseconds";
}

/**
 * @param string $directory
 * @return array
 */
function findCachedFiles(string $directory): array
{
    $directory = realpath($directory);

    $files = findFilesByExtension($directory, '.html');
    $files = array_merge($files, findFilesByExtension($directory, '.css'));
    $files = array_merge($files, findFilesByExtension($directory, '.js'));

    return $files;
}

/**
 * @param string $directory
 * @param string $ext
 * @return array
 */
function findFilesByExtension(string $directory, string $ext): array
{
    if (!endsWith($directory, '/')) {
        $directory .= '/';
    }

    if (endsWith($directory, '/_inc/')) {
        return [];
    }

    $files = [];

    foreach (scandir($directory) as $file) {
        if ($file === '.' || $file === '..') {
            continue;
        }
        $file = $directory . $file;

        if (is_dir($file)) {
            $files = array_merge($files, findCachedFiles($file));
        } else if (endsWith($file, $ext) && !shouldBeIgnored($file)) {
            $files[] = $file;
        }
    }

    return $files;
}