<?php

class FileDoesntExistException extends Exception
{
    /**
     * @param string $hiddenDirectory
     * @return string
     */
    public function getHiddenMessage(string $hiddenDirectory): string
    {
        return str_replace($hiddenDirectory, '', $this->getMessage());
    }
}