<?php

if (!defined('MDCMS_VERSION')) {
    define('MDCMS_VERSION', 'v1.1.0');
}

class ExceptionHandler
{
    const ASCII = " __  __     _  _____ __  __  _____
|  \/  |   | |/ ____|  \/  |/ ____|
| \  / | __| | |    | \  / | (___
| |\/| |/ _` | |    | |\/| |\___ \
| |  | | (_| | |____| |  | |____) |
|_|  |_|\__,_|\_____|_|  |_|_____/ " . MDCMS_VERSION;

    public static function registerHandlers()
    {
        error_reporting(E_ALL & ~E_NOTICE);

        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            require_once dirname(__FILE__) . '/CMSException.php';
            $e = new CMSException();

            $e->setCode($errno);
            $e->setMessage($errstr);
            $e->setFile($errfile);
            $e->setLine($errline);

            ExceptionHandler::handleThrowable($e);
        });
        set_exception_handler('ExceptionHandler::handleThrowable');
    }

    /**
     * @param Throwable $e
     */
    public static function handleThrowable(Throwable $e)
    {
        http_response_code(500);
        header('Content-type: text/plain');

        echo self::ASCII . PHP_EOL . PHP_EOL;
        echo get_class($e) . ': ' . $e->getMessage() . PHP_EOL . PHP_EOL;
        echo $e->getTraceAsString();
        die();
    }

    /**
     * @return array
     */
    public static function loadThemeConfig()
    {
        $theme = [];
        $themeConfig = __DIR__ . '/../themes/' . GLOBAL_CONFIG['theme'] . '/theme.php';
        if (file_exists($themeConfig)) {
            include $themeConfig;
        }

        return $theme;
    }

    /**
     * @param Throwable $e
     * @param string $dirname
     * @param bool $useErrorFile
     */
    public static function handle(Throwable $e, string $dirname, bool $useErrorFile = true)
    {
        $theme = self::loadThemeConfig();

        $errorFile = null;
        $extraHtml = '';

        $errorType = get_class($e);
        $errorMessage = $e->getMessage();
        if ($e instanceof FileDoesntExistException) {
            http_response_code(404);
            if (!empty($theme['errors']) && !empty($theme['errors'][404])) {
                $errorFile = $theme['errors'][404];
            }
            $errorMessage = $e->getHiddenMessage($dirname);

            if (!$errorFile || !file_exists($errorFile)) {
                $errorFile = __DIR__ . '/../themes/default/errors/404.html';

                $extraHtml = '<style>' . file_get_contents(__DIR__ . '/../themes/default/css/default.css') . '</style>';
            }
        } else {
            http_response_code(500);
            if (!empty($theme['errors']) && !empty($theme['errors'][0])) {
                $errorFile = $theme['errors'][0];
            }

            if (!$errorFile || !file_exists($errorFile)) {
                $errorFile = __DIR__ . '/../themes/default/errors/500.html';

                $extraHtml = '<style>' . file_get_contents(__DIR__ . '/../themes/default/css/default.css') . '</style>';
            }
        }

        if ($errorFile && !$extraHtml) {
            if (!startsWith($errorFile, '/')) {
                $errorFile = '/' . $errorFile;
            }
            $errorFile = __DIR__ . '/../themes/' . GLOBAL_CONFIG['theme'] . '/' . $errorFile;
        }

        if (file_exists($errorFile) && $useErrorFile) {
            try {
                $renderer = new FileRenderer($errorFile);

                $renderer->setVariable('{{path}}', $_SERVER['REQUEST_URI']);
                $renderer->setVariable('{{error_type}}', $errorType);
                $renderer->setVariable('{{error_message}}', $errorMessage);

                $html = $renderer->render($extraHtml, $extraHtml ? 'default' : '');

                die($html);
            } catch (Exception $e) {
                self::handle($e, $dirname, false);
            }
        } else {
            header('Content-type: text/plain');

            if (GLOBAL_CONFIG['production']) {
                die('An error occurred');
            }

            echo self::ASCII . PHP_EOL . PHP_EOL;
            die($errorType . ': ' . $errorMessage);
        }
    }
}