<?php

class AssetRenderer
{
    /**
     * @param string $file
     * @throws FileDoesntExistException
     * @throws CMSException
     */
    public static function renderAsset(string $file)
    {
        $startTime = microtime(true);
        $path = normalizePath(dirname(__FILE__) . '/../themes/' . trim(GLOBAL_CONFIG['theme'], '/') . '/' . trim($file, '/'));
        if (!file_exists($path)) {
            throw new FileDoesntExistException('File "' . $path . '" doesn\'t exist');
        }

        $dir = dirname(__FILE__) . '/../../' . trim(pathinfo($file, PATHINFO_DIRNAME), '/') . '/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $ext = trim(pathinfo($file, PATHINFO_EXTENSION), '.');

        switch ($ext) {
            case 'css':
                header('Content-type: text/css');
                break;
            case 'js':
                header('Content-type: text/javascript');
                break;
            default:
                throw new CMSException('Invalid filetype "' . $ext . '" for AssetRenderer');
        }

        $contents = file_get_contents($path);

        file_put_contents($dir . basename($file), $contents);

        $renderTime = formatMicroseconds(microtime(true) - $startTime);
        if (GLOBAL_CONFIG['includeRenderTime']) {
            $contents .= PHP_EOL . "/* Render Time: " . $renderTime . " */";
        }

        die($contents);
    }
}