<?php

class AdminResponse
{
    /**
     * @var string
     */
    public $renderFile = '';

    /**
     * @var array
     */
    public $variables = [];

    /**
     * @var string
     */
    public $redirectTo = '';

    /**
     * @var array
     */
    public $response = [];

    /**
     * AdminResponse constructor.
     * @param string $renderFile
     * @param array $variables
     * @param string $redirectTo
     */
    public function __construct(string $renderFile, array $variables = [], string $redirectTo = '', array $response = [])
    {
        $this->renderFile = $renderFile;
        $this->variables = $variables;
        $this->redirectTo = $redirectTo;
        $this->response = $response;
    }

    /**
     * @param string $renderFile
     * @param array $variables
     * @return AdminResponse
     */
    private static function render(string $renderFile, array $variables = []): AdminResponse
    {
        return new AdminResponse($renderFile, $variables, '', []);
    }

    /**
     * @param string $redirectTo
     * @return AdminResponse
     */
    private static function redirect(string $redirectTo): AdminResponse
    {
        return new AdminResponse('', [], $redirectTo, []);
    }

    /**
     * @param string $data
     * @param string $contentType
     * @return AdminResponse
     */
    private static function respond(string $data, string $contentType): AdminResponse
    {
        return new AdminResponse('', [], '', [
            'data' => $data,
            'contentType' => $contentType
        ]);
    }

    /**
     * @param string $root
     */
    public function check(string $root)
    {
        if ($this->redirectTo) {
            header('Location: admin?/' . $this->redirectTo);
            exit;
        } else if (!empty($this->response)) {
            header('Content-type: ' . $this->response['contentType']);
            die($this->response['data']);
        }

        $this->renderFile = $root . '/_inc/admin/' . $this->renderFile;
    }

    /**
     * @return AdminResponse
     */
    public static function login(): AdminResponse
    {
        if (AdminPanel::isLoggedIn()) {
            return self::redirect('admin');
        }
        return self::render('login.html', [
            'show_error' => isset($_GET['error']) ? '' : 'hide',
        ]);
    }

    /**
     * @return AdminResponse
     */
    public static function doLogin(): AdminResponse
    {
        if (AdminPanel::isLoggedIn()) {
            return self::redirect('admin');
        } else if (empty($_POST['username']) || empty($_POST['password'])) {
            return self::redirect('login');
        }

        if (!AdminPanel::login($_POST['username'], $_POST['password'])) {
            return self::redirect('login&error');
        }
        return AdminResponse::redirect('admin');
    }

    /**
     * @return AdminResponse
     */
    public static function admin(): AdminResponse
    {
        if (!AdminPanel::isLoggedIn()) {
            return self::redirect('login');
        }

        return self::render('admin.html', [
            'username'  => $_SESSION['admin']['username'],
            'l_success' => isset($_GET['l_success']) ? '' : 'hide',
        ]);
    }

    /**
     * @return AdminResponse
     */
    public static function logout(): AdminResponse
    {
        AdminPanel::logout();

        return self::redirect('login');
    }

    public static function doConfigLogin(): AdminResponse
    {
        if (!AdminPanel::isLoggedIn()) {
            return self::redirect('login');
        } else if (empty($_POST['username']) || empty($_POST['password'])) {
            return self::redirect('admin');
        } else if (empty(trim($_POST['username'])) || empty(trim($_POST['password']))) {
            return self::redirect('admin');
        }

        $lines = [
            '// Changes from Admin Panel on ' . date('M jS, Y g:i A'),
        ];

        if (trim($_POST['username']) !== GLOBAL_CONFIG['admin']['username']) {
            $lines[] = '$config[\'admin\'][\'username\'] = \'' . trim($_POST['username']) . '\';';
            $_SESSION['admin']['username'] = trim($_POST['username']);
        }

        $hash = AdminPanel::hash($_POST['username'], $_POST['password']);
        if ($hash !== GLOBAL_CONFIG['admin']['password']) {
            $lines[] = '$config[\'admin\'][\'password\'] = \'' . $hash . '\';';
        }

        if (sizeof($lines) === 1) {
            return self::redirect('admin');
        }

        file_put_contents(__DIR__ . '/../instance.config.php', PHP_EOL . PHP_EOL . implode(PHP_EOL, $lines), FILE_APPEND);

        return self::redirect('admin&l_success');
    }

    /**
     * @return AdminResponse
     */
    public static function clearCache(): AdminResponse
    {
        if (!AdminPanel::isLoggedIn()) {
            return self::respond(json_encode([
                'success' => false,
                'msg' => 'You are not logged in'
            ]), 'application/json');
        }

        $cached = findCachedFiles(__DIR__ . '/../../');
        if (empty($cached)) {
            return self::respond(json_encode([
                'success' => true,
                'msg' => 'No cached files found'
            ]), 'application/json');
        }

        foreach ($cached as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }

        return self::respond(json_encode([
            'success' => true,
            'msg' => 'Successfully deleted ' . sizeof($cached) . ' cached files'
        ]), 'application/json');
    }
}