<?php

class IntegrityChecker
{
    /**
     * @var array
     */
    private static $files = [
        'Classes/ConfigurationException.php',
        'Classes/FileDoesntExistException.php',
        'Classes/FileRenderer.php',
        'Classes/AssetRenderer.php',
        'Classes/ExceptionHandler.php',
        'Classes/AdminPanel.php',

        'vendor/parsedown/Parsedown.php',

        'config.php',
        'instance.config.php',
        'functions.php',

        'themes/default/theme.php',
        'themes/default/footer.html',
        'themes/default/head.html',
        'themes/default/header.html',
        'themes/default/layout.html',

        'themes/default/errors/404.html',
        'themes/default/css/default.css',

        'admin/admin.js',
        'admin/admin.css',
        'admin/admin.html',
        'admin/login.html',
    ];

    /**
     * @var array
     */
    private static $directories = [
        'Classes',
        'themes',
        'themes/default',
        'themes/default/errors',
        'themes/default/css',
        'vendor',
        'vendor/parsedown',
    ];

    /**
     * @var array
     */
    private static $config_keys = [
        'safeMode',
        'escapeHTML',
        'theme',
        'site',
        'ignoreCacheParameter',
        'includeRenderTime',
        'minifyHTML',
        'externalTargetBlank',
        'production',
        'admin',
    ];

    public static function check()
    {
        $rootFolder = rtrim(realpath(dirname(__FILE__) . '/../'), '/') . '/';

        $errors = [];

        foreach (self::$files as $file) {
            if (!file_exists($rootFolder . $file)) {
                $errors[] = 'Missing file "' . $rootFolder . $file . '"';
            } else if (!is_file($rootFolder . $file)) {
                $errors[] = '"' . $rootFolder . $file . '" is not a file';
            }
        }

        foreach (self::$directories as $directory) {
            if (!file_exists($rootFolder . $directory)) {
                $errors[] = 'Missing directory "' . $rootFolder . $directory . '"';
            } else if (!is_dir($rootFolder . $directory)) {
                $errors[] = '"' . $rootFolder . $directory . '" is not a directory';
            }
        }

        include_once dirname(__FILE__) . '/../config.php';
        if (!defined('GLOBAL_CONFIG')) {
            $errors[] = '"GLOBAL_CONFIG" is not defined';
        }

        foreach (self::$config_keys as $key) {
            if (!defined('GLOBAL_CONFIG') || !isset(GLOBAL_CONFIG[$key])) {
                $errors[] = 'GLOBAL_CONFIG["' . $key . '"] is not set';
            }
        }

        if (!empty($errors)) {
            http_response_code(500);
            header('Content-type: text/plain');

            echo 'SELFCHECK FAILED' . PHP_EOL . PHP_EOL;
            die(implode(PHP_EOL, $errors));
        }
    }
}