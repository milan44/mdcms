<?php

class AdminPanel
{
    /**
     * Returns a hash for the given username and password
     *
     * @param string $username
     * @param string $password
     * @return string
     */
    public static function hash(string $username, string $password): string
    {
        return hash_pbkdf2('sha256', trim($username), trim($password), 1000);
    }

    /**
     * Checks if the given username and password are valid and sets the responsive session
     *
     * @param string $username
     * @param string $password
     * @return bool
     */
    public static function login(string $username, string $password): bool
    {
        $hash = self::hash($username, $password);

        if ($username === GLOBAL_CONFIG['admin']['username'] && $hash === GLOBAL_CONFIG['admin']['password']) {
            $_SESSION['admin'] = [
                'username' => $username,
            ];

            return true;
        }
        return false;
    }

    /**
     * Logs the current user out
     */
    public static function logout()
    {
        session_destroy();
    }

    /**
     * Checks if the current user is logged in
     *
     * @return bool
     */
    public static function isLoggedIn(): bool
    {
        return !empty($_SESSION['admin']) && !empty($_SESSION['admin']['username']);
    }

    /**
     * @return AdminResponse
     */
    public static function handle(): AdminResponse
    {
        $page = array_keys($_GET);

        if (empty($page)) {
            $page = [
                '/login'
            ];
        }

        switch ($page[0]) {
            case '/login':
                return AdminResponse::login();
            case '/doLogin':
                return AdminResponse::doLogin();
            case '/admin':
                return AdminResponse::admin();
            case '/logout':
                return AdminResponse::logout();
            case '/doConfigLogin':
                return AdminResponse::doConfigLogin();
            case '/clearCache':
                return AdminResponse::clearCache();
            default:
                header('Location: admin');
                exit;
        }
    }
}