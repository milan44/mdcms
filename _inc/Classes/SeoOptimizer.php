<?php

class SeoOptimizer
{
    const DefaultTags = [
        'og:type'      => 'website',
        'twitter:card' => 'summary',
    ];

    /**
     * @var array
     */
    private $variables;

    public function __construct(array $variables)
    {
        $this->variables = $variables;
    }

    /**
     * @return string
     */
    public function renderMetaTags(): string
    {
        if (empty($this->variables['_seo']) || !is_array($this->variables['_seo'])) {
            return '';
        }
        $seo = $this->variables['_seo'];

        $tags = self::DefaultTags;

        if (empty($seo['title']) && !empty($this->variables['title'])) {
            $seo['title'] = $this->variables['title'];
        }

        if (!empty($seo['title'])) {
            $tags['title'] = $seo['title'];
            $tags['og:title'] = $seo['title'];
            $tags['twitter:title'] = $seo['title'];
        }
        if (!empty($seo['description'])) {
            $tags['description'] = $seo['description'];
            $tags['og:description'] = $seo['description'];
            $tags['twitter:description'] = $seo['description'];
        }
        if (!empty($seo['image'])) {
            if (is_array($seo['image']) && !empty($seo['image']['url'])) {
                $tags['og:image'] = $seo['image']['url'];
                $tags['twitter:image'] = $seo['image']['url'];

                if (!empty($seo['image']['alt'])) {
                    $tags['og:image:alt'] = $seo['image']['alt'];
                    $tags['twitter:image:alt'] = $seo['image']['alt'];
                }
            } else if (is_string($seo['image'])) {
                $tags['og:image'] = $seo['image'];
                $tags['twitter:image'] = $seo['image'];
            }
        }

        if (!empty($seo['extra']) && is_array($seo['extra'])) {
            foreach ($seo['extra'] as $key => $val) {
                if (!empty($val) && is_string($val)) {
                    $tags[$key] = $val;
                } else if (!empty($val) && is_array($val)) {
                    $tags[$key] = json_encode($val);
                }
            }
        }

        $grouped = [];
        foreach ($tags as $key => $val) {
            $group = explode(':', $key);
            $namespace = sizeof($group) > 1 ? $group[0] : '_';

            if (!isset($grouped[$namespace])) {
                $grouped[$namespace] = [];
            }

            $grouped[$namespace][$key] = $val;
        }

        ksort($grouped);

        $html = [];
        foreach ($grouped as $group => $tags) {
            ksort($tags);

            foreach ($tags as $key => $val) {
                $html[] = '<meta ' . ($group === '_' ? 'name' : 'property') . '="' . htmlentities($key) . '" content="' . htmlentities($val) . '" />';
            }

            $html[] = '';
        }

        return trim(implode(PHP_EOL, $html));
    }
}