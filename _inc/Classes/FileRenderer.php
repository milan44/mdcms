<?php

class FileRenderer
{
    /**
     * @var string
     */
    private $markdown;

    /**
     * @var array
     */
    private $properties;

    /**
     * @var array
     */
    private $variables;

    /**
     * @var int
     */
    private $startTime = 0;

    /**
     * @var SeoOptimizer
     */
    private $seoOptimizer;

    /**
     * FileRenderer constructor.
     * @param string $file
     * @throws ConfigurationException
     * @throws FileDoesntExistException
     */
    public function __construct(string $file, array $customVariables = [])
    {
        $this->startTime = microtime(true);

        if (!file_exists($file)) {
            throw new FileDoesntExistException('File "' . $file . '" doesn\'t exist');
        }

        $contents = file_get_contents($file);

        if (startsWith($contents, '{')) {
            $json = '';
            $lines = explode("\n", $contents);
            foreach ($lines as &$line) {
                if (trim($line) === '---') {
                    $line = '';
                    break;
                }
                $json .= $line;
                $line = '';
            }
            $this->markdown = trim(implode("\n", $lines));
            $json = json_decode($json, true);
            if (!is_array($json) && $json !== []) {
                throw new ConfigurationException('File "' . $file . '" has an invalid configuration');
            }

            $this->properties = $this->combine(GLOBAL_CONFIG['site'], $json);
        } else {
            $this->markdown = trim($contents);
            $this->properties = GLOBAL_CONFIG['site'];
        }

        if (!empty($customVariables)) {
            $this->properties = $this->combine($this->properties, $customVariables);
        }

        $this->variables = $this->parseVariables($this->properties);

        $this->seoOptimizer = new SeoOptimizer($this->properties);
    }

    /**
     * @param string $html
     * @return string
     */
    private function retargetLinks(string $html): string
    {
        if (!GLOBAL_CONFIG['externalTargetBlank']) {
            return $html;
        }

        return preg_replace_callback('/(?:<\s*a.*?href=[\'"])(.*?)(?:[\'"].*?>)/si', function ($m) {
            $h = $m[0];
            if (preg_match('/target=[^\s]+/si', $h)) {
                return $h;
            }

            if (sizeof($m) > 1 && preg_match('/^(https?:)?\/\//mi', $m[1])) {
                $h = preg_replace('/>$/m', ' target="_blank">', $h);
            }
            return $h;
        }, $html);
    }

    /**
     * @param string $html
     * @return string
     */
    private function tidyOutput(string $html): string
    {
        if (!GLOBAL_CONFIG['minifyHTML']) {
            return $html;
        }

        $minifier = new TinyHtmlMinifier([
            'collapse_whitespace' => false,
            'disable_comments' => true
        ]);

        return $minifier->minify($html);
    }

    /**
     * @param string $extraHtml
     * @param string $overrideTheme
     * @return string
     * @throws FileDoesntExistException
     */
    public function render(string $extraHtml = '', string $overrideTheme = ''): string
    {
        $theme = $overrideTheme ?: trim(GLOBAL_CONFIG['theme'], '/');

        $requiredFiles = [
            dirname(__FILE__) . '/../themes/' . $theme . '/head.html',
            dirname(__FILE__) . '/../themes/' . $theme . '/header.html',
            dirname(__FILE__) . '/../themes/' . $theme . '/footer.html',
            dirname(__FILE__) . '/../themes/' . $theme . '/layout.html'
        ];

        foreach ($requiredFiles as $file) {
            if (!file_exists($file)) {
                throw new FileDoesntExistException('File "' . $file . '" doesn\'t exist');
            }
        }

        $parsedown = new Parsedown();
        $parsedown->setMarkupEscaped(GLOBAL_CONFIG['escapeHTML']);
        $parsedown->setSafeMode(GLOBAL_CONFIG['safeMode']);

        $lines = explode("\n", $this->markdown);
        $body = [];
        $md = [];
        $lastIndent = '';
        foreach ($lines as $line) {
            if (startsWith(ltrim($line), '<')) {
                $lastIndent = str_replace(ltrim($line), '', $line);
                if (!empty($md)) {
                    $body[] = $parsedown->text(implode("\n", $md));
                    $md = [];
                }
                $body[] = $parsedown->line(ltrim($line));
            } else {
                if (strlen($lastIndent) > 0) {
                    $line = preg_replace('/^\s{1,' . strlen($lastIndent) . '}/', '', $line);

                    if (empty($md)) {
                        $lastIndent .= str_replace(ltrim($line), '', $line);
                        $line = ltrim($line);
                    }
                }

                $md[] = $line;
            }
        }
        if (!empty($md)) {
            $body[] = $parsedown->text(implode("\n", $md));
        }
        $body = implode("\n", $body);

        $head = file_get_contents($requiredFiles[0]) . PHP_EOL . $this->seoOptimizer->renderMetaTags();
        $header = file_get_contents($requiredFiles[1]);
        $footer = file_get_contents($requiredFiles[2]);

        $layout = file_get_contents($requiredFiles[3]);

        $this->replaceVariables($layout, $this->variables);
        $this->replaceVariables($head, $this->variables);
        $this->replaceVariables($header, $this->variables);
        $this->replaceVariables($footer, $this->variables);
        $this->replaceVariables($body, $this->variables);

        $rendered = str_replace('###HEAD###', $head, $layout);
        $rendered = str_replace('###HEADER###', $header, $rendered);
        $rendered = str_replace('###BODY###', $body, $rendered);
        $rendered = str_replace('###FOOTER###', $footer, $rendered);

        if (!empty(GLOBAL_CONFIG['ignoreCacheParameter'])) {
            $rendered = str_ireplace("</body>", "<script>
        (function() {
            var field = '" . urlencode(GLOBAL_CONFIG['ignoreCacheParameter']) . "';
            var url = window.location.href;
            var path = window.location.pathname;
            if (path.lastIndexOf('/renderer.php', 0) === 0) return;
            if (url.indexOf('?' + field + '=1') !== -1 || url.indexOf('&' + field + '=1') !== -1)
                window.location.replace('/renderer.php?path=' + encodeURI(path));
        })();
    </script>
</body>", $rendered);
        }

        $rendered = $this->retargetLinks($rendered);

        $rendered = $this->tidyOutput($rendered);

        $rendered = str_ireplace('</body>', $extraHtml . '</body>', $rendered);

        $renderTime = formatMicroseconds(microtime(true) - $this->startTime);
        if (GLOBAL_CONFIG['includeRenderTime']) {
            $rendered = str_ireplace("</body>", "<!-- Render Time: " . $renderTime . " --></body>", $rendered);
        }

        return $rendered;
    }

    public function setVariable(string $key, string $value)
    {
        $this->variables[$key] = $value;
    }

    /**
     * @param string $html
     * @param array $variables
     */
    private function replaceVariables(string &$html, array $variables)
    {
        foreach ($variables as $key => $value) {
            $html = str_ireplace($key, $value, $html);
        }

        $html = str_replace('\\{', '{', $html);
        $html = str_replace('\\}', '}', $html);
    }

    /**
     * @param array $variables
     * @param string $prefix
     * @return array
     */
    private function parseVariables(array $variables, string $prefix = ''): array
    {
        $parsed = [];
        foreach ($variables as $key => $value) {
            $key = $prefix . $key;
            if (is_array($value)) {
                $parsed = array_merge($parsed, $this->parseVariables($value, $key . '.'));
            } else {
                $parsed['{{' . $key . '}}'] = $value;
            }
        }
        return $parsed;
    }

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    private function combine(array $array1, array $array2): array
    {
        foreach ($array2 as $key => $value) {
            if (isset($array1[$key]) && is_array($array1[$key])) {
                $array1[$key] = $this->combine($array1[$key], $value);
            } else {
                $array1[$key] = $value;
            }
        }
        return $array1;
    }
}