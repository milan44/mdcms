(function() {
    function getJson(url) {
        return new Promise(function(resolve, reject) {
            let xmlHttp = new XMLHttpRequest();

            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState === XMLHttpRequest.DONE) {
                    if (xmlHttp.status === 200) {
                        try {
                            resolve(JSON.parse(xmlHttp.responseText));
                        } catch(e) {
                            reject();
                        }
                    } else {
                        reject();
                    }
                }
            };

            xmlHttp.open("GET", url, true);
            xmlHttp.send();
        });
    }

    let cacheMessage = document.getElementById('cacheMessage');
    document.getElementById('clearCache').addEventListener('click', function(e) {
        e.preventDefault();

        if (this.classList.contains('disabled')) {
            return;
        }
        this.classList.add('disabled');
        let _this = this;

        cacheMessage.classList.remove('alert-success');
        cacheMessage.classList.remove('alert-error');
        cacheMessage.innerText = 'Clearing cache...';

        getJson('admin?/clearCache').then(function(data) {
            cacheMessage.classList.remove('hide');
            cacheMessage.innerText = data.msg;

            if (data.success) {
                cacheMessage.classList.add('alert-success');
            } else {
                cacheMessage.classList.add('alert-error');
            }
            _this.classList.remove('disabled');
        }).catch(function() {
            cacheMessage.classList.remove('hide');
            cacheMessage.classList.add('alert-error');

            cacheMessage.innerText = 'Something went wrong while trying to clear the cache';
            _this.classList.remove('disabled');
        });
    }, false);
})();