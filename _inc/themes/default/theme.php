<?php

$theme = [
    'errors' => [
        0   => 'errors/500.html', // Generic error page
        404 => 'errors/404.html' // 404 error page
    ],
];