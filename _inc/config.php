<?php
session_start();

$config = [
    'site' => [
        'title' => 'My MdCMS Website',
        '_seo'  => [
            'title'       => 'My MdCMS Website', // If empty will default to site.title
            'description' => 'This is the description of my first MdCMS powered website.',
            'image'       => [
                'url' => '/images/example.png',
                'alt' => 'A fluffy animal hanging from a rope',
            ],
            'extra'       => [ // Any extra meta tags
                               'og:site_name' => 'MdCMS Example',
            ],
        ],
    ],

    'production' => false, // Turn this to false if you want to have detailed error messages

    'theme' => 'default', // Theme you want to use (you can add new themes in the themes folder)

    'directoryIndex'       => 'index', // Directory index page
    'ignoreCacheParameter' => 'no_cache', // GET Parameter which (if provided) will force a re-render of the current page
    'includeRenderTime'    => true, // Include a small html comment documenting the rendering time
    'minifyHTML'           => true, // If the output html should be minified

    'escapeHTML'          => false, // Escape all html inside markup files
    'safeMode'            => false, // Apply sanitization to additional scripting vectors (such as scripting link destinations) that are introduced by the markdown syntax itself
    'externalTargetBlank' => true, // Add target="_blank" to external links

    'mappings' => [ // Map a path to a different markdown file, path has to be absolute
        '/index' => 'README.md'
    ],

    'ignoreCache' => [ // HTML/CSS/JS files that should not be deleted by clearing the cache
        'my-special-file.html'
    ],

    'admin' => [
        'username' => 'admin', // Admin panel username
        'password' => '92bf91eaf351f1e7879b2fd09e8cb1536905ea43f0af594d6e0b23560c7c2f15', // Username + Password as a hash, default password is 'password'. this can be changed from the admin panel
    ],
];

require_once __DIR__ . '/instance.config.php';

define('GLOBAL_CONFIG', $config);