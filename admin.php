<?php
require_once __DIR__ . '/_inc/Classes/IntegrityChecker.php';
IntegrityChecker::check();

require_once __DIR__ . '/_inc/Classes/ExceptionHandler.php';
ExceptionHandler::registerHandlers();

require_once __DIR__ . '/_inc/functions.php';

$adminCss = file_get_contents(__DIR__ . '/_inc/themes/default/css/default.css');
$adminCss .= PHP_EOL . file_get_contents(__DIR__ . '/_inc/admin/admin.css');

$adminJs = file_get_contents(__DIR__ . '/_inc/admin/admin.js');

$response = AdminPanel::handle();
$response->check(__DIR__);

try {
    $renderer = new FileRenderer($response->renderFile, $response->variables);
    $html = $renderer->render('<style>' . $adminCss . '</style><script>' . $adminJs . '</script>', 'default');

    $html = preg_replace('/<link[^>]+rel="stylesheet"[^>]+\/?>/miU', '', $html);

    die($html);
} catch (Exception $e) {
    ExceptionHandler::handle($e, dirname(__FILE__));
}